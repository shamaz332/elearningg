import React from 'react'
import {graphql} from 'gatsby'
import Header from '../components/Courses/header.js'
import SideCard from '../components/Courses/SideCard.js'
import Styles from './temp.module.css'
import DropDown from '../components/Courses/DropDown.js'
import Fade from 'react-reveal/Fade'
import Description from '../components/Courses/description.js'
import Layout from '../components/Layout/Layout'

export default function temp ({data}) {
    const {title,description,sections,track,quarter}=data.contentfulCourse
    const srcpath=track!==undefined ? track.map(val=>val.program.map(i=>i.image.fluid.src)) : ["//images.ctfassets.net/6y7x6a0he6ux/fnmiegLZP7y4t5vqUvrsl/1c229ee250ce3e2e2f7b989e8b51b50c/ai.jpeg?w=800&q=50"]
    const paths=srcpath["0"].map(val=>val)

    return (
        <Layout>
            <div className={Styles.head} style={{marginTop: "7rem"}}>
                <Header paths={paths} title={title} description={description} sections={sections}/>
            </div>
            <div className={Styles.side}>
                <SideCard weeks={sections.weeks} quiz={sections.length} url={paths}/>
            </div>
            <div className={Styles.contain}>
                <div className={Styles.cert}>
                    <p>Congratulation! You Have Successfully Completed this Course</p>

                </div>        
                <Description description={description}/>
                <Fade bottom><DropDown quarter={quarter} sections={sections}/></Fade>
                <br/><br/>
            </div>
        </Layout>

    )
}

export const pagedata=graphql `
	query($id:String!){
		contentfulCourse(id:{eq:$id}){
			title
			description{
				description
			}
            quarter
			sections{
				weeks
				title
				quiz{
					week
					title
		}
        }
            track {
                program {
                image {
                    fluid {
                    src
                    }
                }
                }
            }	
	
	}
}`
;
