import React from "react"
import { useTheme } from "@material-ui/core/styles"
import {
  Grid,
  Card,
  CardContent,
  Typography,
  Button,
  CardActions,
} from "@material-ui/core"
import SwipeableViews from "react-swipeable-views"
import { autoPlay } from "react-swipeable-views-utils"
import Rating from "@material-ui/lab/Rating"
import Avatar from "@material-ui/core/Avatar"
import classes from "./css/Reviews.module.css"

const AutoPlaySwipeableViews = autoPlay(SwipeableViews)
// <<<<<<<<<<<<<<<<<<<<<Dummy Data>>>>>>>>>>>>>>>>>>>>>>>>
const tutorialSteps = [
  {
    label: "MUHAMMAD NAEEM AKHTAR",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text:
      "As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel across her face and She is the host to your journey.",
  },
  {
    label: "SAAD BAIG",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1538032746644-0212e812a9e7?auto=format&fit=crop&w=400&h=250&q=60",
    text:
      "As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel across her face and She is the host to your journey.",
  },
  {
    label: "ALI HASSAN",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537996194471-e657df975ab4?auto=format&fit=crop&w=400&h=250&q=80",
    text:
      "As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel across her face and She is the host to your journey.",
  },
  {
    label: "MUHAMMAD ANNAS",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1518732714860-b62714ce0c59?auto=format&fit=crop&w=400&h=250&q=60",
    text:
      "As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it, you travel across her face and She is the host to your journey.",
  },
  {
    label: "Abdur Rehman",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text: "MCS-Freelance MERN/PHP developer",
  },
  {
    label: "Shamaz",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text: "MCS-Freelance MERN/PHP developer",
  },
  {
    label: "Salman",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text: "MCS-Freelance MERN/PHP developer",
  },
  {
    label: "Fiaz Ahmad",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text: "MCS-Freelance MERN/PHP developer",
  },
  {
    label: "Maryam",
    rating: 5,
    imgPath:
      "https://images.unsplash.com/photo-1537944434965-cf4679d1a598?auto=format&fit=crop&w=400&h=250&q=60",
    text: "MCS-Freelance MERN/PHP developer",
  },
]

// <<<<<<<<<<<<<<<<<<<<<Data End>>>>>>>>>>>>>>>>>>>>>>>>

export default function Coursel() {
  const theme = useTheme()
  const [activeStep, setActiveStep] = React.useState(0)

  const handleStepChange = step => {
    setActiveStep(step)
  }

  return (
    <div className={classes.container}>
      <h1 className={classes.heading}>Our Testimonials</h1>
      <Grid container justify="center">
        <Grid item component={Card} xs={12} md={4} className={classes.card}>
          <AutoPlaySwipeableViews
            axis={theme.direction === "rtl" ? "x-reverse" : "x"}
            index={activeStep}
            onChangeIndex={handleStepChange}
            enableMouseEvents
          >
            {tutorialSteps.map((step, index) => (
              <div key={step.label}>
                {Math.abs(activeStep - index) <= 2 ? (
                  <div>
                    <CardActions>
                      <Button>
                        <Avatar
                          src={step.imgPath}
                          className={classes.large}
                        ></Avatar>
                      </Button>
                      <Button className={classes.label}>
                        <h3>{step.label}</h3>
                      </Button>
                    </CardActions>
                    {/* <CardHeader
                          avatar={
                            <Avatar src={step.imgPath} className={classes.large} ></Avatar>
                          }
                          title={step.label}
                        /> */}
                    <CardContent>
                      <Typography className={classes.contents}>
                        {step.text}
                      </Typography>
                      <Rating name="simple-controlled" value={step.rating} />
                    </CardContent>
                  </div>
                ) : //<img className={classes.img} src={step.imgPath} alt={step.label} />
                null}
              </div>
            ))}
          </AutoPlaySwipeableViews>
        </Grid>
      </Grid>
    </div>
  )
}
