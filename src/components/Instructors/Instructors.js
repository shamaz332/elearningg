import React from "react"
import Avatar from "@material-ui/core/Avatar"
import classes from "./css/Instructors.module.css"
import zk from "./images/zia.jpg"
import aa from "./images/adil2.jpeg"
import zh from "./images/zh.jpg"
import mamhira from "./images/mamhira.png"

export default function Instructors() {
  return (
    <div className={classes.container}>
      <p className={classes.p1}>
        Our <strong>Instructors</strong>
      </p>
      <div className={classes.flexContainer}>
        <div>
          <Avatar src={zk} alt="Zia Khan" className={classes.avatar}>
            Zia Khan
          </Avatar>
          <p className={classes.p2}>
            Zia <strong>Khan</strong>
          </p>
        </div>
        <div>
          <Avatar src={aa} alt="Adil Altaf" className={classes.avatar}>
            Adil Altaf
          </Avatar>
          <p className={classes.p2}>
            Adil <strong>Altaf</strong>
          </p>
        </div>
        <div>
          <Avatar src={zh} alt="Zeeshan Hanif" className={classes.avatar}>
            Zeeshan Hanif
          </Avatar>
          <p className={classes.p2}>
            Zeeshan <strong>Hanif</strong>
          </p>
        </div>
        <div>
          <Avatar src={mamhira} alt="Hira Khan" className={classes.avatar}>
            Hira Khan
          </Avatar>
          <p className={classes.p2}>
            Hira <strong>Khan</strong>
          </p>
        </div>
      </div>
    </div>
  )
}
