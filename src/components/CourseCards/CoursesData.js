import ai from "../../images/ai.jpg"
import dataAnalysis from "../../images/data-analysis.jpg"
import deep from "../../images/machine-learning.jpg"
import iot from "../../images/iot.jpg"
import cloudiot from "../../images/cloud-iot.jpg"
import blockchain from "../../images/blockchain.jpg"
import mern from "../../images/mern.jpg"
import cloud from "../../images/cloud-computing.jpg"
import kubernetes from "../../images/kubernetes.png"

// <<<<<<<<<<<<<<<<<<<<<Dummy data>>>>>>>>>>>>>>>>>>>>>>>>

export const data = [
  {
    imageUrl: ai,
    name: "AI For Everyone",
    line:
      "We will start this course by understand the fundamentals and use cases for AI...Read more",
    id: 1,
  },
  {
    imageUrl: dataAnalysis,
    name: "Data Analysis",
    line:
      "We will start the course by learning NumPy the backbone of scientific computing...Read more",
    id: 2,
  },
  {
    imageUrl: deep,
    name: "Deep Learning",
    line:
      "We will start this course by focusing on deep learning for computer vision...Read more",
    id: 3,
  },
  {
    imageUrl: iot,
    name: "Internet of Things",
    line:
      "We will start this course by introducing IoT and embedded systems and move on to learn the Rust systems...Read more",
    id: 4,
  },
  {
    imageUrl: cloudiot,
    name: "Cloud Native Computing for IoT",
    line:
      "We will learn how to develop cloud-based microservies using Docker, Kubernetes...Read more",
    id: 5,
  },
  {
    imageUrl: blockchain,
    name: "Blockchain Business Foundation",
    line:
      "This course will prepare the student for the Pearson VUE Certified Blockchain Business Foundations Exam (CBBF)....Read more",
    id: 6,
  },
  {
    imageUrl: mern,
    name: "MERN Stack",
    line:
      "In this course we will cover JAMstack and MERN stack development...Read more",
    id: 7,
  },
  {
    imageUrl: cloud,
    name: "Cloud Native Computing",
    line:
      "Linux containers are poised to take over the world; we will start this quarter with an introduction of Linux...Read more",
    id: 8,
  },
  {
    imageUrl: kubernetes,
    name: "Kubernetes",
    line:
      "In this course we will prepare for Certification in Kubernetes...Read more",
    id: 9,
  },
]
