import React, { useState } from "react"
import {
  Typography,
  Card,
  CardActionArea,
  CardActions,
  CardMedia,
  Button,
  CardContent,
} from "@material-ui/core"
import { useStyles } from "./StyleCards"
import StarRatings from "react-star-ratings"

export function Cards({ name, url, line }) {
  const classes = useStyles()

  const [rating, setRating] = useState()

  const changeRating = (newRating, name) => {
    setRating(newRating)
  }

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia className={classes.media} image={url} title={name} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {line}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.cardActions}>
        <Button size="small" color="primary">
          Enroll
        </Button>
        <Button size="small" color="primary">
          Read More
        </Button>
        <StarRatings
          rating={rating}
          starRatedColor="blue"
          changeRating={changeRating}
          numberOfStars={5}
          name="rating"
          starDimension="22px"
          starSpacing="3px"
          starHoverColor="yellow"
          starRatedColor="yellow"
        />
      </CardActions>
    </Card>
  )
}
