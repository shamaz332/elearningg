import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      fontFamily: 'Montserrat, sans-serif',
    },
    media: {
      height: 200,
    },
    cardActions: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }
});
