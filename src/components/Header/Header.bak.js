import { Link } from "gatsby"
import React from "react"
import logo  from '../../images/logo.png';
import {  Navbar, Nav, Container,  NavItem } from 'react-bootstrap';
import NavbarCollapse from "react-bootstrap/NavbarCollapse";
import style from './Header.module.css';
import { Person } from '@material-ui/icons';
import cx from 'classnames';
export const Header = () => {
    return (
        <header className={style.bg} >
            <Container >
                <Navbar className={style.bg} expand="lg">
                    <Navbar.Brand className={style.navbrand}>
                        <img src={logo} className={style.logo} alt="logo" />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-aria-controls="navbarResponsive" />
                    <NavbarCollapse id="navbarResponsive">
                        <Nav as="ul"  className={cx("mr-auto", style.ulelements)}>
                            <Nav.Item as="li" >
                                <Link to="/" className="nav-link" activeClassName="active">Home</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">Courses</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">Pages</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">ShortCodes</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">Blog</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">Gallery</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/faq" className="nav-link" activeClassName="active">FAQ</Link>
                            </Nav.Item>
                            <Nav.Item as="li">
                                <Link to="/page" className="nav-link" activeClassName="active">Contact US</Link>
                            </Nav.Item>
                        </Nav>
                        <Nav as="ul" className={cx(style.login, style.ulelements)}>
                            <NavItem as="li">
                                <span className={style.loginreg}><Link to="/login" className={style.link}><Person /> Login</Link> | Register</span>
                            </NavItem>
                        </Nav>
                    </NavbarCollapse>
                </Navbar>
            </Container>
            <div className={style.bgImage}>
                <div className={style.centered}>
                    Training Innovative 4IR Software Developers for the Industry and Getting Sri Lanka ready for Post Covid-19 economy
                </div>
            </div>
        </header>
    )
}
export default Header;