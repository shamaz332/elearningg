import React from "react"
import style from "./css/Header.module.css"
import { Button } from "@material-ui/core"
export const Header_Background = () => {
  return (
    <div>
      <div className={style.bgImage}></div>
      <div className={style.centered}>
        <h1>Training Innovative 4IR Software Developers</h1>
        <h4>
          For the Industry and Getting Sri Lanka ready for Post Covid-19 economy
        </h4>
      </div>
      <Button variant="contained" className={style.btncentered}>
        Start Learning
      </Button>
    </div>
  )
}
export default Header_Background
