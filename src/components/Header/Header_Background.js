import React from "react"
import style from "./css/Header_Background.module.css"
import { Button } from "@material-ui/core"
import headerpic from "../../images/header/header_pic.png"
export const Header_Background = () => {
  return (
    <div>
      <div className={style.bgImage}>
        <div className={style.centered}>
          <div className={style.disp_header_pic}>
            <img src={headerpic} className={style.img_knowledge} />
          </div>

          <div>
            <span className={style.lbl_clr}>Trainning</span>&nbsp;{" "}
            <span className={style.lbl_sec_clr}>Innovative</span>
            <br />
            <span className={style.lbl_sec_clr}>Software</span> &nbsp;
            <span className={style.lbl_clr}>Developers</span>
            <br />
            <br />
            <Button variant="contained" className={style.btn}>
              Start Learning Now
            </Button>
          </div>
        </div>
      </div>
    </div>
  )
}
export default Header_Background
