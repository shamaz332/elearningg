import React from "react"
import Footer from "../Footer/Footer"
import Header from "../Navbar/Header"

import { ThemeProvider } from "@material-ui/core/styles"

import { theme } from "../Footer/styles/theme"
import classes from "./css/Layout.module.css"

export default function Layout({ children }) {
  return (
    <div className={classes.container}>
      <Header />
      {children}
      <ThemeProvider theme={theme}>
        <Footer />
      </ThemeProvider>
    </div>
  )
}
