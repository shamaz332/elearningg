import React from "react"
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import Typography from "@material-ui/core/Typography"
import ApartmentOutlinedIcon from "@material-ui/icons/ApartmentOutlined"
import BusinessCenterOutlinedIcon from "@material-ui/icons/BusinessCenterOutlined"
import BrightnessAutoOutlinedIcon from "@material-ui/icons/BrightnessAutoOutlined"

import styles from "./Features.module.css"

export default function Features() {
  return (
    <div className={styles.container}>
      <h1 className={styles.heading}>
        Features That Make Us <span style={{ color: "#FFCB9A" }}>Hero</span>
      </h1>

      <h4 className={styles.heading}>
        If you are looking at blank cassettes on the web, you may be very
        confused at the difference in price. <br />
        You may see some for as low as $.17 each.
      </h4>
      <Grid container justify="center">
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent className={styles.cardContent}>
            <Typography>
              <ApartmentOutlinedIcon className={styles.icon64} />
              <h2 className={styles.headingCard}>Architecture</h2>
              Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do
              eiusmod tempor incididunt labore.
            </Typography>
          </CardContent>
        </Grid>
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent className={styles.cardContent}>
            <Typography>
              <BusinessCenterOutlinedIcon className={styles.icon64} />
              <h2 className={styles.headingCard}>Interior Design</h2>
              Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do
              eiusmod tempor incididunt labore.
            </Typography>
          </CardContent>
        </Grid>
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent className={styles.cardContent}>
            <Typography>
              <BrightnessAutoOutlinedIcon className={styles.icon64} />
              <h2 className={styles.headingCard}>Concept Design</h2>
              Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do
              eiusmod tempor incididunt labore.
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </div>
  )
}
