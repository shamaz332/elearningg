import React from "react"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import Heading from "./Heading"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  aboutUsBtn: {
    marginTop: theme.spacing(4),
    border: "2px solid black",
    fontWeight: "bold",
  },
  text: {
    padding: "0.5rem 0",
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const AboutUs = () => {
  const classes = useStyles()

  return (
    <>
      <Heading text="About us" />
      <div className={classes.text}>
        <Typography variant="body2" color="textPrimary">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. A est
          distinctio similique libero inventore voluptate asperiores? Fuga error
          facilis dolorum?
        </Typography>
        <br />
        <Typography variant="body2" color="textPrimary">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa
          incidunt laborum numquam cum dolor temporibus labore odit officia non
          asperiores, recusandae aliquid tempora id accusamus deserunt beatae
          assumenda quasi cupiditate.
        </Typography>
      </div>
    </>
  )
}

export default AboutUs
