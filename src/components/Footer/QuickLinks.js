import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import { Link } from "gatsby"
import Heading from "./Heading"
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos"
import { theme } from "./styles/theme"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  item: {
    padding: "1rem 0",
    border: `0px solid ${theme.palette.secondary.main}`,
    borderBottomWidth: "2px",
    width: "100%",

    "&:last-child": {
      border: "none",
    },
  },

  link: {
    textDecoration: "none",
    display: "flex",
    alignItems: "center",
    color: theme.palette.text.secondary,
    transition: "0.5s",

    "&:hover": {
      color: theme.palette.text.primary,
    },
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const Item = ({ name, linkTo }) => {
  const classes = useStyles()

  return (
    <Grid item xs={12} className={classes.item}>
      <Link to={linkTo} className={classes.link}>
        <ArrowForwardIosIcon style={{ fontSize: 12 }} />
        <Typography variant="button" style={{ marginLeft: theme.spacing(2) }}>
          {name}
        </Typography>
      </Link>
    </Grid>
  )
}

const QuickLinks = () => {
  return (
    <>
      <Heading text="Quick Links" />

      <Grid container direction="column">
        <Item linkTo="/Courses" name="Courses" />

        <Item linkTo="/Instructors" name="Instructors" />

        <Item linkTo="/blog" name="Blog" />

        <Item linkTo="/gallery" name="Gallery" />

        <Item linkTo="/faq" name="FAQs" />

        <Item linkTo="/contact" name="Contact us" />
      </Grid>
    </>
  )
}

export default QuickLinks
