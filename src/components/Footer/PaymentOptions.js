import React from "react"
import {
  FaCcMastercard,
  FaCcVisa,
  FaCcPaypal,
  FaCcApplePay,
  FaCcAmazonPay,
} from "react-icons/fa"
import { makeStyles } from "@material-ui/core"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "flex-start",
    color: theme.palette.text.secondary,
    marginTop: theme.spacing(2),
  },

  icon: {
    marginRight: theme.spacing(2),
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const PaymentOptions = () => {
  const classes = useStyles()

  const size = 40
  return (
    <div className={classes.root}>
      <FaCcMastercard size={size} className={classes.icon} />
      <FaCcVisa size={size} className={classes.icon} />
      <FaCcPaypal size={size} className={classes.icon} />
      <FaCcApplePay size={size} className={classes.icon} />
      <FaCcAmazonPay size={size} className={classes.icon} />
    </div>
  )
}

export default PaymentOptions
