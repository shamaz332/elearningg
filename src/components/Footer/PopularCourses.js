import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import { Link } from "gatsby"
import Heading from "./Heading"
import StarBorderIcon from "@material-ui/icons/StarBorder"
import StarIcon from "@material-ui/icons/Star"
import StarHalfIcon from "@material-ui/icons/StarHalf"
import Rating from "@material-ui/lab/Rating"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  item: {
    padding: "0.5rem 0",
    border: `0px solid ${theme.palette.secondary.main}`,
    borderBottomWidth: "2px",
    width: "100%",

    "&:last-child": {
      border: "none",
    },
  },

  link: {
    textDecoration: "none",
    color: theme.palette.text.primary,

    "&:hover": {
      color: `${theme.palette.text.secondary} `,
    },
  },

  courseContainer: {
    [theme.breakpoints.only("xs")]: {
      flexDirection: "column-reverse",
      padding: `16px 0`,
    },
  },

  ratings: {
    color: theme.palette.text.secondary,
  },

  cover: {
    objectFit: "cover",
    height: "auto",
    width: "100%",

    boxShadow: "2px 2px 8px 0px rgba(0,0,0,0.258)",
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const Item = ({ name, price, thumbnailSrc, linkTo }) => {
  const classes = useStyles()

  return (
    <Grid item xs={12} className={classes.item}>
      <Link to={linkTo} className={classes.link}>
        <Grid
          container
          direction="row"
          spacing={1}
          justify="space-between"
          className={classes.courseContainer}
        >
          <Grid item xs={12} sm={7}>
            <Typography variant="body1" gutterBottom>
              {name}
            </Typography>
            <div className={classes.ratings}>
              <Rating name="simple-controlled" value={4} />
            </div>
            <Typography>{price === 0 ? "Free" : "$ " + price}</Typography>
          </Grid>
          <Grid item xs={12} sm={5}>
            <img
              className={classes.cover}
              src={thumbnailSrc}
              alt="course thumbnail"
            />
          </Grid>
        </Grid>
      </Link>
    </Grid>
  )
}

const PopularCourses = () => {
  return (
    <>
      <Heading text="Popular Courses" />
      <Grid container direction="column">
        <Item
          name="Artificial Intelligence"
          price={40}
          linkTo="/"
          thumbnailSrc="https://i.ytimg.com/vi/JMUxmLyrhSk/maxresdefault.jpg"
        />
        <Item
          name="Cloud Native Computing"
          price={0}
          linkTo="/"
          thumbnailSrc="https://www.webmagspace.com/wp-content/uploads/2019/11/1.jpg"
        />
        <Item
          name="Internet of things"
          price={0}
          linkTo="/"
          thumbnailSrc="https://i.ytimg.com/vi/LlhmzVL5bm8/maxresdefault.jpg"
        />
      </Grid>
    </>
  )
}

export default PopularCourses
