import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import LocationOnIcon from "@material-ui/icons/LocationOn"
import EmailIcon from "@material-ui/icons/Email"
import CallIcon from "@material-ui/icons/Call"
import MessageIcon from "@material-ui/icons/Message"
import Heading from "./Heading"
import { makeStyles } from "@material-ui/core/styles"

// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  item: {
    padding: "0.5rem",
    border: `0px solid ${theme.palette.divider}`,
    borderBottomWidth: "1px",
    width: "100%",

    "&:last-child": {
      border: "none",
    },
    color: theme.palette.grey["600"],
  },

  link: {
    textDecoration: "none",
    color: theme.palette.text.error,
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const ContactUs = () => {
  const classes = useStyles()

  return (
    <>
      <Heading text="Contact us" />
      <Grid container direction="column" justify="center">
        <Grid item xs={12} container direction="row" className={classes.item}>
          <Grid item xs={2}>
            <LocationOnIcon color="secondary" />
          </Grid>
          <Grid item xs={10}>
            <Typography>
              The&nbsp;Design&nbsp;Themes&nbsp;Inc.
              <br />
              Mary&nbsp;Jane&nbsp;St,&nbsp;Sydney&nbsp;2233
              <br />
              Australia
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={12} container direction="row" className={classes.item}>
          <Grid item xs={2}>
            <CallIcon color="secondary" />
          </Grid>
          <Grid item xs={10}>
            <a className={classes.link} href="tel:123-456-7890">
              +12&nbsp;(3)&nbsp;456&nbsp;7890
            </a>
          </Grid>
        </Grid>
        <Grid item xs={12} container direction="row" className={classes.item}>
          <Grid item xs={2}>
            <MessageIcon color="secondary" />
          </Grid>
          <Grid item xs={10}>
            <Typography variant="body2">
              +12&nbsp;(3)&nbsp;456&nbsp;7890
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={12} container direction="row" className={classes.item}>
          <Grid item xs={2}>
            <EmailIcon color="secondary" />
          </Grid>
          <Grid item xs={10}>
            <a className={classes.link} href="mailto: abc@example.com">
              abc@example.com
            </a>
          </Grid>
        </Grid>
      </Grid>
    </>
  )
}

export default ContactUs
