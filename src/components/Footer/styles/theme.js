import { createMuiTheme } from "@material-ui/core/styles"

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#116466",
      dark: "#00393c",
    },
    secondary: {
      main: "#2C3531",
    },
    error: {
      light: "#ffb56b",
      main: "#DA853D",
      dark: "#a4580c",
      contrastText: "#ffffff",
    },
    background: {
      default: "#D1E8E2",
    },

    text: {
      primary: "#ffffff",
      secondary: "black",
      error: "#DA853D",
    },
  },
  typography: {
    htmlFontSize: 14,
  },
})
