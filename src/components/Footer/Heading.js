import React from "react"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles(theme => ({
  heading: {
    marginBottom: theme.spacing(2),
  },
}))

const Heading = ({ text }) => {
  const classes = useStyles()

  return (
    <Typography variant="h5" color="textSecondary" className={classes.heading}>
      {text}
    </Typography>
  )
}
export default Heading
