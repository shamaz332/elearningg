import React from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import IconButton from "@material-ui/core/IconButton"
import { FaTwitter, FaYoutube, FaFacebook, FaSkype } from "react-icons/fa"
import { makeStyles } from "@material-ui/core/styles"
import ScopedCssBaseline from "@material-ui/core/ScopedCssBaseline"
import QuickLinks from "./QuickLinks"
import PopularCourses from "./PopularCourses"
import PaymentOptions from "./PaymentOptions"
import AboutUs from "./AboutUs"
import { Container } from "@material-ui/core"
import { theme } from "./styles/theme"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  footer: {
    width: "100%",
    backgroundColor: theme.palette.background.default,
  },

  top: {
    paddingTop: "7rem",
    width: "100%",
    position: "relative",
    overflow: "hidden",
    backgroundSize: "cover",
    background: "url('footer_bg.svg') top center ",

    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(8),
      background: 'center center, url("footer_bg-sm.svg") top center',
    },

    [theme.breakpoints.down("xs")]: {
      paddingTop: theme.spacing(2),
      background: 'center center, url("footer_bg-xs.svg") top center no-repeat',
    },

    "&::before": {
      content: '""',
      position: "absolute",
      padding: "0",
      margin: 0,
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      background: 'url("dots_bg_o.png") top center repeat',

      clipPath:
        "polygon(0% 32%, 32% 13%, 61% 3%, 85% 20%, 100% 50%, 100% 100%, 0% 100%)",

      [theme.breakpoints.only("md")]: {
        clipPath:
          "polygon(0 27%, 26% 14%, 61% 3%, 86% 11%, 100% 28%, 100% 100%, 0 100%)",
      },

      [theme.breakpoints.only("sm")]: {
        clipPath:
          "polygon(0 13%, 23% 6%, 62% 0, 86% 5%, 100% 9%, 100% 100%, 0 100%)",
      },

      [theme.breakpoints.only("xs")]: {
        clipPath:
          "polygon(0 9%, 24% 4%, 62% 0, 84% 2%, 100% 9%, 100% 100%, 0 100%)",
      },
    },
  },

  topContainer: {
    position: "relative",
    zIndex: 1,
    width: "70%",
    padding: "5rem 0",
    [theme.breakpoints.down("md")]: {
      padding: "5rem 2rem",
      width: "100%",
    },
  },

  bottom: {
    width: "100%",
    backgroundColor: theme.palette.primary.dark,
  },

  bottomContainer: {
    width: "70%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "1rem 0",

    [theme.breakpoints.down("md")]: {
      padding: "1.5rem 1rem",
      flexDirection: "column",
      alignContent: "space-between",
    },
  },

  socialContainer: {
    [theme.breakpoints.down("md")]: {
      marginTop: theme.spacing(1),
    },
  },

  socialBtn: {
    transition: "0.75s",
    color: theme.palette.background.default,

    "&:hover": {
      color: theme.palette.text.secondary,
    },
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const Footer = () => {
  const classes = useStyles()

  return (
    <ScopedCssBaseline>
      <footer className={classes.footer}>
        <div className={classes.top}>
          <Container className={classes.topContainer}>
            <Grid container spacing={4}>
              <Grid item xs={12} md={4}>
                <AboutUs />
                <PaymentOptions />
              </Grid>
              <Grid item xs={12} md={3}>
                <QuickLinks />
              </Grid>
              <Grid item xs={12} md={5}>
                <PopularCourses />
              </Grid>
            </Grid>
          </Container>
        </div>

        <div className={classes.bottom}>
          <Container className={classes.bottomContainer}>
            <Typography
              variant="body1"
              align="center"
              style={{ color: theme.palette.background.default }}
            >
              Copyright &copy; 2020 Ninja&nbsp;Team
              All&nbsp;Rights&nbsp;Reserved
            </Typography>

            <div className={classes.socialContainer}>
              <IconButton className={classes.socialBtn}>
                <FaTwitter />
              </IconButton>
              <IconButton className={classes.socialBtn}>
                <FaYoutube />
              </IconButton>
              <IconButton className={classes.socialBtn}>
                <FaFacebook />
              </IconButton>
              <IconButton className={classes.socialBtn}>
                <FaSkype />
              </IconButton>
            </div>
          </Container>
        </div>
      </footer>
    </ScopedCssBaseline>
  )
}

export default Footer
