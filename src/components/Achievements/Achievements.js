import React from "react"
import Grid from "@material-ui/core/Grid"
import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import Typography from "@material-ui/core/Typography"
import LanguageOutlinedIcon from "@material-ui/icons/LanguageOutlined"
import LocalLibraryOutlinedIcon from "@material-ui/icons/LocalLibraryOutlined"
import GroupAddOutlinedIcon from "@material-ui/icons/GroupAddOutlined"
import CountUp from "react-countup"
import styles from "./Achievements.module.css"
import cx from "classnames"

export default function Achievements() {
  return (
    <div className={cx(styles.container, styles.achievementBg)}>
      <h1 className={styles.achievement}>
        Center <span style={{ color: "#FFCB9A" }}>Achievements</span>
      </h1>

      <h3 className={styles.achievement}>
        Here you can review some statistics about our Education Center
      </h3>
      <Grid container spacing={3} justify="center">
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent>
            <Typography className={styles.achievement}>
              <LanguageOutlinedIcon className={styles.icon64} />
              <h1 className="achievement">
                <CountUp
                  style={{ color: "#FFCB9A" }}
                  start={0}
                  end={100328}
                  duration={2}
                  separator=","
                />
              </h1>
              <h2>
                Foreign <span style={{ color: "#FFCB9A" }}>Followers</span>
              </h2>
            </Typography>
          </CardContent>
        </Grid>
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent>
            <Typography className={styles.achievement}>
              <LocalLibraryOutlinedIcon className={styles.icon64} />
              <h1 className="achievement">
                <CountUp
                  style={{ color: "#FFCB9A" }}
                  start={0}
                  end={549}
                  duration={2}
                  separator=","
                />
              </h1>
              <h2>
                Classes <span style={{ color: "#FFCB9A" }}>Complete</span>
              </h2>
            </Typography>
          </CardContent>
        </Grid>
        <Grid item component={Card} xs={12} md={3} className={styles.card}>
          <CardContent>
            <Typography className={styles.achievement}>
              <GroupAddOutlinedIcon className={styles.icon64} />
              <h1 className="achievement">
                {" "}
                <CountUp
                  style={{ color: "#FFCB9A" }}
                  start={0}
                  end={6789}
                  duration={2}
                  separator=","
                />
              </h1>
              <h2>
                Students <span style={{ color: "#FFCB9A" }}>Enrolled</span>
              </h2>
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </div>
  )
}
