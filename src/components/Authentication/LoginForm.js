import React from "react"
import Typography from "@material-ui/core/Typography"
import {
  TextField,
  Checkbox,
  FormControlLabel,
  Button,
} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import { Link } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: "500px",
    margin: "auto",
  },

  heading: {
    marginBottom: "2rem",
  },

  form: {
    display: "flex",
    flexDirection: "column",
  },
  topMargin: {
    marginTop: theme.spacing(2),
  },
  link: {
    textDecoration: "none",
    color: theme.palette.text.error,
    textAlign: "center",
    marginTop: "1rem",
  },
}))

const LoginForm = () => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Typography variant="h4" className={classes.heading}>
        Login Form
      </Typography>
      <Typography variant="body1">Already a Member? Log in here.</Typography>
      <form className={classes.form}>
        <TextField id="username" label="Username" fullWidth required />
        <TextField
          id="password"
          label="Password"
          fullWidth
          type="password"
          required
        />
        {/* <Checkbox label="Remember me" /> */}
        <FormControlLabel
          className={classes.topMargin}
          control={<Checkbox name="remember" />}
          label="Remember me"
        />
        <Button
          className={classes.topMargin}
          variant="contained"
          color="secondary"
        >
          Login
        </Button>
        <Link to="/" className={classes.link}>
          Lost your password?
        </Link>
      </form>
    </div>
  )
}

export default LoginForm
