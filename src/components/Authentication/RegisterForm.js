import React from "react"
import Typography from "@material-ui/core/Typography"
import {
  TextField,
  Select,
  Button,
  InputLabel,
  MenuItem,
  FormControl,
} from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles(theme => ({
  root: {
    margin: "auto",
    width: "100%",
    maxWidth: "500px",
  },

  heading: {
    marginBottom: "2rem",
  },

  form: {
    display: "flex",
    flexDirection: "column",
  },
  topMargin: {
    marginTop: theme.spacing(2),
  },
}))

const RegisterForm = () => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Typography variant="h4" className={classes.heading}>
        Register Form
      </Typography>
      <Typography variant="body1">
        Do not have an account? Register here.
      </Typography>
      <form className={classes.form}>
        <TextField id="username2" label="Username" fullWidth required />
        <TextField id="email" label="email" fullWidth type="email" required />

        <FormControl required className={classes.topMargin}>
          <InputLabel id="role">Role</InputLabel>
          <Select
            labelId="role"
            id="role-select"
            value="subscriber"
            // onChange={() => {}}
          >
            <MenuItem value="subscriber">Subscriber</MenuItem>
            <MenuItem value="teacher">Teacher</MenuItem>
            <MenuItem value="student">Student</MenuItem>
          </Select>
        </FormControl>

        <Button
          style={{ marginTop: "2rem" }}
          variant="contained"
          color="secondary"
        >
          Register
        </Button>
      </form>
    </div>
  )
}

export default RegisterForm
