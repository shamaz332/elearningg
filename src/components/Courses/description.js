import React from "react"
import Styles from "./css/description.module.css"
import Fade from "react-reveal/Fade"

export default function Description(props) {
  return (
    <Fade bottom>
      <div className={Styles.desc}>
        <p>{props.description.description}</p>
      </div>
    </Fade>
  )
}
