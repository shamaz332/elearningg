import React from "react"
import Carousel from "react-multi-carousel"
import "react-multi-carousel/lib/styles.css"
import CatlogCard from "./Card.js"
import ai from "./Images/ai.jpeg"
import iot from "./Images/iot.jpeg"
import cloud from "./Images/cloud.jpeg"
import blockchain from "./Images/blockchain.jpeg"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
}
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
export default function Slider() {
  return (
    <Carousel responsive={responsive}>
      <CatlogCard
        path={ai}
        name={"Artificial Intelligence"}
        info={
          "The One Year Micro Degree For Absolute Beginners. Transforming Industries with 4IRU "
        }
      />
      <div>
        <CatlogCard
          path={cloud}
          name={"Cloud Native Computing"}
          info={
            "The One Year Micro Degree For Absolute Beginners. Transforming Industries with 4IRU "
          }
        />
      </div>
      <div>
        <CatlogCard
          path={blockchain}
          name={"BlockChain Development"}
          info={
            "The One Year Micro Degree For Absolute Beginners. Transforming Industries with 4IRU "
          }
        />
      </div>
      <div>
        <CatlogCard
          path={iot}
          name={"Internet of things"}
          info={
            "The One Year Micro Degree For Absolute Beginners. Transforming Industries with 4IRU "
          }
        />
      </div>
    </Carousel>
  )
}
