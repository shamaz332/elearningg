import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import CardMedia from "@material-ui/core/CardMedia"
import CardContent from "@material-ui/core/CardContent"
import CardActions from "@material-ui/core/CardActions"
import Typography from "@material-ui/core/Typography"
import Styles from "./css/Courses.module.css"
import StarRatings from "react-star-ratings"
import Rating from "@material-ui/lab/Rating"
import { Link } from "gatsby"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 360,
    minWidth: "35%",
    minHeight: "450px",
    border: "0.5px solid #ccc",
    backgroundColor: "transparent",
    marginLeft: "20px",
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
export default function CatlogCard(props) {
  const classes = useStyles()

  return (
    <Card className={classes.root} style={{ height: "200px" }}>
      <CardMedia className={classes.media} image={props.path} />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          <h3>{props.name}</h3>
          {props.info}
        </Typography>
      </CardContent>
      <Link to="Courses">
        <CardActions disableSpacing>
          <button className={Styles.gocourse}>Check Course</button>
        </CardActions>
      </Link>
      <br />
      <div className={Styles.wraprate}>
        <h4 className={Styles.rate}>
          Ratings
          <br />
          <p>4.5/5 Stars</p>
        </h4>

        <Rating name="simple-controlled" value={4} />
      </div>
    </Card>
  )
}
