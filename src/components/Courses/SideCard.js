import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardActions from "@material-ui/core/CardActions"
import CardContent from "@material-ui/core/CardContent"
import CardMedia from "@material-ui/core/CardMedia"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import Styles from "./css/SideCard.module.css"
import Fade from "react-reveal/Fade"
import { FiBook } from "react-icons/fi"
import { GrDocumentText } from "react-icons/gr"
import { BsQuestionCircle } from "react-icons/bs"
import { FaCertificate } from "react-icons/fa"

// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles({
  root: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
  media: {
    height: 250,
  },
})
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
export default function SideCard(props) {
  const { quiz, url } = props
  const path = `https:${url["0"]}`

  const classes = useStyles()
  return (
    <Fade bottom>
      <div className={Styles.card}>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={path}
              title="Contemplative Reptile"
            />
            <CardContent>
              <CardActions style={{ marginLeft: "10px" }}>
                <Button
                  size="large"
                  color="primary"
                  style={{
                    border: "2px solid black",
                    width: "330px",
                    borderRadius: "20px",
                  }}
                >
                  Buy
                </Button>
                <br />
              </CardActions>
              <CardActions style={{ marginLeft: "55px" }}>
                <Button
                  size="large"
                  color="primary"
                  style={{ width: "250px", borderRadius: "20px" }}
                >
                  Open Registration
                </Button>
                <br />
              </CardActions>

              <Typography
                style={{
                  fontSize: "20px",
                  marginTop: "7px",
                  marginLeft: "15px",
                }}
              >
                Course Includes:
              </Typography>
              <Typography
                style={{
                  fontSize: "16px",
                  marginTop: "7px",
                  marginLeft: "25px",
                }}
              >
                <FiBook size={25} />
                &nbsp;&nbsp;6 Lessons
              </Typography>
              <Typography
                style={{
                  fontSize: "16px",
                  marginTop: "7px",
                  marginLeft: "25px",
                }}
              >
                <GrDocumentText size={25} />
                &nbsp;&nbsp;11 Topics
              </Typography>
              <Typography
                style={{
                  fontSize: "16px",
                  marginTop: "7px",
                  marginLeft: "25px",
                }}
              >
                <BsQuestionCircle size={25} />
                &nbsp;&nbsp;{`${quiz} Quizes`}
              </Typography>
              <Typography
                style={{
                  fontSize: "16px",
                  marginTop: "7px",
                  marginLeft: "25px",
                }}
              >
                <FaCertificate size={25} />
                &nbsp;&nbsp;Course Certification
              </Typography>
            </CardContent>
          </CardActionArea>
          <br />
          <br />
        </Card>
      </div>
    </Fade>
  )
}
