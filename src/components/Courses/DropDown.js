import React,{useState} from "react"
import Styles from './css/dropdown.module.css'
import Fade from 'react-reveal/Fade'
import {FaCheckCircle} from 'react-icons/fa'
export default function DropDown(props){
	const [drop,setdrop]=useState(false)


	return(
		<div className={Styles.dropdown}>
			{props.sections.map(val=>
			<>
			<Fade bottom><div className={Styles.drop} onClick={()=>setdrop(true)}>
			<p>{val.title}</p>
			<FaCheckCircle className={Styles.check} size={25} color="green"/>
			</div></Fade>
			<div className={Styles.cont}>
			{drop===true &&
			<>
			<p>Week:&nbsp;&nbsp;
			{drop===true &&
					val.weeks

			}	
			</p>
						
			<p>Quiz:&nbsp;&nbsp;
			{val.quiz!==null && drop===true &&
						val.quiz.title
					}
			</p>
			</>
		}
			</div>
			</>
		)}
			
		</div>
		)
}