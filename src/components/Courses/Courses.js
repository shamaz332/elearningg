import React from "react"
import Styles from "./css/Courses.module.css"
import Slider from "./Slider"
import Fade from "react-reveal"
import { Link } from "gatsby"

export default function CoursesContainer() {
  //console.log(Styles)
  return (
    <div className={Styles.contain}>
      <div className={Styles.infocont}>
        <Fade bottom>
          <h1>
            Popular <span style={{ color: "#FFCB9A" }}>Courses</span>
          </h1>
        </Fade>
        <Fade bottom>
          <p className={Styles.p1}>
            Aspire an Acomplish! Find your interests by browsing our online
            Courses
          </p>
        </Fade>
        <Fade bottom>
          <p className={Styles.p1}>Upgrade your skills, level up in life.</p>
        </Fade>
        <Fade bottom>
          <Link to="Courses">
            <div style={{ textAlign: "center" }}>
              <button className={Styles.coursebtn}>View All Courses</button>
            </div>
          </Link>
        </Fade>
      </div>
      <Fade right>
        <div className={Styles.slide}>
          <Slider />
        </div>
      </Fade>
    </div>
  )
}
