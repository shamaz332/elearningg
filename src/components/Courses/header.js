import React from "react"
import Styles from "./css/Header.module.css"
import Fade from "react-reveal"

export default function Header(props) {
  const { paths, title } = props
  console.log(paths["0"])
  const path = `https:${paths["0"]}`
  console.log(path)
  return (
    <div
      className={Styles.main}
      style={{
        backgroundImage: `url(${path})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPositionY: "-200px",
      }}
    >
      <div className={Styles.head}>
        <Fade bottom>
          <h1>{title}.</h1>
          <button>View Details</button>
        </Fade>
      </div>
    </div>
  )
}
