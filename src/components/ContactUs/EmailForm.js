import React from "react"
import Typography from "@material-ui/core/Typography"
import { TextField, Button } from "@material-ui/core"
import { makeStyles } from "@material-ui/core/styles"
import AccountCircle from "@material-ui/icons/AccountCircle"
import Email from "@material-ui/icons/Email"
import PhoneAndroidRounded from "@material-ui/icons/PhoneAndroidRounded"
import MessageRounded from "@material-ui/icons/MessageRounded"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  heading: {
    marginBottom: "2rem",
    textTransform: "uppercase",
    border: `0px solid`,
    borderBottomWidth: "2px",
  },

  form: {
    display: "flex",
    flexDirection: "column",
  },

  formGroup: {
    display: "flex",
    alignItems: "flex-start",
    marginTop: theme.spacing(2),
  },

  textField: {
    marginLeft: theme.spacing(2),
    color: "black",
    "& label.Mui-focused": {
      color: "#116466",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#116466 !important",
    },
  },

  topMargin: {
    marginTop: theme.spacing(2),
  },
  link: {
    textDecoration: "none",
    // color: theme.palette.text.error,
    textAlign: "center",
    marginTop: "1rem",
  },
  btn: {
    marginTop: theme.spacing(2),
    width: "160px",
    alignSelf: "flex-end",
    color: "#2C3531",
    // backgroundColor: "#FFCB9A",
    "&:hover": {
      color: "#FFCB9A",
      backgroundColor: "#2C3531",
    },
  },
  icon: {
    fontSize: "2rem",
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const EmailForm = () => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Typography variant="h5" className={classes.heading}>
        Give us a message
      </Typography>

      <form className={classes.form}>
        <div className={classes.formGroup}>
          <AccountCircle
            className={classes.icon}
            style={{ color: "#116466" }}
          />

          <TextField
            className={classes.textField}
            variant="filled"
            id="name"
            label="Your Name"
            fullWidth
          />
        </div>
        <div className={classes.formGroup}>
          <Email className={classes.icon} style={{ color: "#116466" }} />

          <TextField
            id="emailaddress"
            label="Email"
            className={classes.textField}
            variant="filled"
            fullWidth
          />
        </div>
        <div className={classes.formGroup}>
          <PhoneAndroidRounded
            className={classes.icon}
            style={{ color: "#116466" }}
          />

          <TextField
            id="phonenumber"
            label="Phone"
            className={classes.textField}
            variant="filled"
            fullWidth
          />
        </div>
        <div className={classes.formGroup}>
          <MessageRounded
            className={classes.icon}
            style={{ color: "#106466" }}
          />

          <TextField
            id="message"
            label="Message"
            multiline
            rows={4}
            className={classes.textField}
            variant="filled"
            fullWidth
          />
        </div>
        <Button
          className={classes.btn}
          variant="contained"
          fullWidth={false}
          style={{ backgroundColor: "#116466", color: "white" }}
        >
          Send email
        </Button>
      </form>
    </div>
  )
}

export default EmailForm
