import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import { Typography } from "@material-ui/core"

// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(8),
    width: "100%",
  },
  container: {
    padding: "1rem",
    width: "70%",
    margin: "auto",

    [theme.breakpoints.down("md")]: {
      width: "100%",
    },
  },
  name: {
    textTransform: "uppercase",
  },
}))
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
const Header = ({ name }) => {
  const classes = useStyles()
  return (
    <div className={classes.container}>
      <Typography
        className={classes.name}
        variant="h4"
        style={{ textAlign: "center" }}
      >
        {name}
      </Typography>
    </div>
  )
}
export default Header
