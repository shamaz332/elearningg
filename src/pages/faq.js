import React, { useState } from "react"
import faqData from "../components/hook/useFAQData"
import { withStyles } from "@material-ui/core/styles"
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel"
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary"
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails"
import { Button, Box, Typography } from "@material-ui/core"
import Container from "@material-ui/core/Container"
import Layout from "../components/Layout/Layout"
import { Helmet } from "react-helmet"
import styles from "./css/faq.module.css"
import ContactSupportOutlinedIcon from "@material-ui/icons/ContactSupportOutlined"
import { typography } from "@material-ui/system"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 2,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiExpansionPanel)

const ExpansionPanelSummary = withStyles({
  expandIcon: {
    order: -1,
  },

  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "2px solid rgba(0, 0, 0, .125)",
    marginBottom: 4,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary)

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
}))(MuiExpansionPanelDetails)
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
export default function Faq() {
  const faqDataa = faqData()
  const [value, setValue] = useState("")
  const [expanded, setExpanded] = React.useState("panel1")

  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false)
  }

  return (
    <Layout>
      <Helmet>
        <title>FAQ</title>
      </Helmet>
      <div>
        {/* <Typography variant="h4" align="center" style={{ padding: "20px" }}>
          Frequently Asked Questions
        </Typography> */}

        <Box
          className={styles.box}
          data-sal="fade"
          data-sal-delay="500"
          data-sal-easing="ease"
          data-sal-duration="1000"
        >
          <Typography variant="h2" className={styles.boxHeading}>
            Frequently Asked Questions
          </Typography>
        </Box>
        <Container>
          <form className={styles.form} noValidate autoComplete="off">
            <input
              type="search"
              value={value}
              placeholder="Search question"
              className={styles.input}
              onChange={e => setValue(e.target.value)}
            />
            <Button variant="contained" className={styles.searchBtn}>
              Search
            </Button>
          </form>
          {faqDataa.map((faq, index) => (
            <ExpansionPanel
              square
              expanded={expanded === "panel1" + index}
              onChange={handleChange("panel1" + index)}
              key={index}
            >
              <ExpansionPanelSummary
                aria-controls="panel1d-content"
                id="panel1d-header"
                className={styles.border}
                expandIcon={<ContactSupportOutlinedIcon />}
                IconButtonProps={{ edge: "start" }}
              >
                <Typography
                  className={styles.typography}
                  dangerouslySetInnerHTML={{
                    __html: faq.heading.childMarkdownRemark.html,
                  }}
                ></Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography
                  dangerouslySetInnerHTML={{
                    __html: faq.description.childMarkdownRemark.html,
                  }}
                ></Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          ))}
        </Container>
        {/* first question */}
      </div>
    </Layout>
  )
}
