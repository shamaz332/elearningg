import React from "react"
import Grid from "@material-ui/core/Grid"
import { makeStyles } from "@material-ui/core/styles"
import Header from "../components/ContactUs/Header"
import EmailForm from "../components/ContactUs/EmailForm"
import ContactInfo from "../components/ContactUs/ContactInfo"
import "./css/contact.css"
import Layout from "../components/Layout/Layout"
import { Helmet } from "react-helmet"

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: "#D1E8E2",
    padding: "80px 0px",
    marginTop: "80px",
  },

  container: {
    width: "70%",
    margin: "auto",
  },
}))

export default function ContactPage() {
  const classes = useStyles()
  return (
    <Layout>
      <Helmet>
        <title>Contact US</title>
      </Helmet>
      <div className={classes.root}>
        <Header name="Contact" />
        <Grid container spacing={4} className={classes.container}>
          <Grid item xs={12} md={8}>
            <EmailForm />
          </Grid>
          <Grid item xs={12} md={4}>
            <ContactInfo />
          </Grid>
        </Grid>
      </div>
    </Layout>
  )
}
