import React from "react"

// Other Component
import Courses from "../components/Courses/Courses"
import Achievements from "../components/Achievements/Achievements"
import Features from "../components/Features/Features"
import Reviews from "../components/Reviews/Reviews"

// CSS imported
import "bootstrap/dist/css/bootstrap.min.css"
import Layout from "../components/Layout/Layout.js"
import { Header_Background } from "../components/Header/Header_Background"
import classes from "./css/index.module.css"
import Instructors from "../components/Instructors/Instructors"
import { Helmet } from "react-helmet"

export default function Home() {
  return (
    <Layout>
      <Helmet>
        <title>Home</title>
      </Helmet>
      <Header_Background />
      <Instructors />
      <div className={classes.bg}>
        <Courses />
        <Achievements />
        <Features />
      </div>
      <Reviews />
    </Layout>
  )
}
