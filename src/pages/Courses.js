import React, { useState } from "react"
import {
  Box,
  Typography,
  Button,
  Grid,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
} from "@material-ui/core"
import Layout from "../components/Layout/Layout"
import { makeStyles } from "@material-ui/core/styles"
import cover from "../images/cover5.jpg"
import { graphql } from "gatsby"
import { Link } from "gatsby"
import StarRatings from "react-star-ratings"
import Fade from "react-reveal/Fade"
import { Helmet } from "react-helmet"
// <<<<<<<<<<<<<<<<<<<<<styling>>>>>>>>>>>>>>>>>>>>>>>>
const useStyles = makeStyles({
  container: {
    // border: '1px solid black',
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  // BOX 1
  box: {
    backgroundImage: `url(${cover})`,
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "fixed",
    height: "350px",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  boxHeading: {
    background: "rgba(0, 0, 0, 0.5)",
    height: "100%",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: "#ffffff",
    fontWeight: "bold",
    fontFamily: "Montserrat",
    letterSpacing: "0.1em",
  },
  // BOX 2
  box2: {
    width: "80%",
    paddingTop: "30px",
  },
  box2Text: {
    fontFamily: "Montserrat, sans-serif",
    color: "white",
  },
  // FORM
  form: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "15px",
    paddingBottom: "30px",
  },
  input: {
    padding: "6px 16px",
    fontSize: "0.875rem",
    marginRight: "5px",
    borderRadius: "5px",
    border: "1px solid lightgray",
    height: "40px",
    width: "60%",
  },
  searchBtn: {
    boxShadow: "none",
    height: "40px",
    backgroundColor: "#FFCB9A",
    color: "#2C3531",
    "&:hover": {
      backgroundColor: "#2C3531",
      color: "#FFCB9A",
    },
  },
  // GRID CONTAINER
  gridContainer: {
    width: "100%",
    height: "100%",
    marginTop: "15px",
    marginBottom: "15px",
    paddingTop: "15px",
    paddingBottom: "15px",
  },
  gridItem: {
    margin: 10,
    padding: 0,
  },

  // CARDS

  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    maxWidth: "100%",
    fontFamily: "Montserrat, sans-serif",
    height: "380px",
    borderRadius: "10px",
    backgroundColor: "#2C3531",
    color: "white",
  },
  cardActionsArea: {
    // height: '80%',
  },
  media: {
    height: 200,
  },
  cardActions: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    borderTop: "1px solid black",
  },
})
// <<<<<<<<<<<<<<<<<<<<<styling End>>>>>>>>>>>>>>>>>>>>>>>>
export default function CoursesM({ data }) {
  const classes = useStyles()
  const { edges } = data.allContentfulCourse
  //const [filteredData, setFilteredData] = useState(data)
  const [value, setValue] = useState("")

  // Star ratings
  const [rating, setRating] = useState()

  const changeRating = (newRating, name) => {
    setRating(newRating)
  }

  return (
    <Layout>
      <Helmet>
        <title>Courses</title>
      </Helmet>
      <div className={classes.container}>
        {/* BOX 1 */}
        <Box className={classes.box}>
          <Typography variant="h2" className={classes.boxHeading}>
            Courses
          </Typography>
        </Box>
        <form className={classes.form} noValidate autoComplete="off">
          <input
            type="search"
            value={value}
            placeholder="Search course"
            className={classes.input}
            onChange={e => setValue(e.target.value)}
          />
          <Button variant="contained" className={classes.searchBtn}>
            Search
          </Button>
        </form>

        {/* GRID CONTAINER AND CARDS */}
        <Grid
          container
          spacing={2}
          className={classes.gridContainer}
          justify="center"
          alignItems="center"
        >
          {edges.map(edge => (
            <Grid
              className={classes.gridItem}
              key={edge.node.id}
              xl={3}
              lg={3}
              md={4}
              sm={6}
              xs={10}
            >
              <Link
                to={`/Courses/${edge.node.title}`}
                style={{ textDecoration: "none", color: "black" }}
              >
                <Fade left>
                  <Card className={classes.root}>
                    <CardActionArea className={classes.cardActionsArea}>
                      <CardMedia
                        className={classes.media}
                        image={`http:${edge.node.track[0].program[0].image.fluid.src}`}
                        title={edge.node.title}
                      />
                      <CardContent style={{ paddingBottom: 0 }}>
                        <Typography gutterBottom variant="h6" component="h2">
                          {edge.node.title}
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    <CardActions className={classes.cardActions}>
                      <Button size="small" style={{color: "white"}}>
                        Enroll
                      </Button>
                      <Button size="small" style={{color: "white"}}>
                        Read More
                      </Button>
                      <StarRatings
                        rating={rating}
                        starRatedColor="blue"
                        changeRating={changeRating}
                        numberOfStars={5}
                        name="rating"
                        starDimension="22px"
                        starSpacing="3px"
                        starHoverColor="yellow"
                        starRatedColor="yellow"
                      />
                    </CardActions>
                  </Card>
                </Fade>
              </Link>
            </Grid>
          ))}
        </Grid>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query MyQuery {
    allContentfulCourse {
      edges {
        node {
          title
          id
          description {
            description
          }
          track {
            program {
              image {
                fluid {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
