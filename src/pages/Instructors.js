import React, { Fragment } from "react"
import insStyle from "./css/Instructors.module.css"
import Layout from "../components/Layout/Layout"
import { Box, Typography } from "@material-ui/core"
import fac2 from "../images/instructor/zia.jpg"
import fac3 from "../images/instructor/adil2.jpeg"
import zeeshan from "../images/instructors/zh.jpg"
import { FaFacebookF, FaTelegramPlane, FaLinkedinIn } from "react-icons/fa"

function Instructor() {
  return (
    <Fragment>
      <Layout>
        <Box
          className={insStyle.box}
          data-sal="fade"
          data-sal-delay="500"
          data-sal-easing="ease"
          data-sal-duration="1000"
        >
          <Typography variant="h2" className={insStyle.boxHeading}>
            Instructor
          </Typography>
        </Box>
        <div className={insStyle.FacultyContent}>
          <div>
            <div className={insStyle.FacultyBanner}>
              LEARN FROM THE INDUSTRY EXPERT'S
            </div>
          </div>
          <div className={insStyle.FacultyMember}>
            <div class={insStyle.cardcontainer}>
              <span class={insStyle.pro}>HEAD</span>
              <img class={insStyle.round} src={fac2} alt="user" />
              <h3>ZIA KHAN</h3>
              <h6>PROGRAM HEAD</h6>
              <p>MSE, MBA, MAC & M.A Economics</p>
              <div class={insStyle.button}>
                <FaFacebookF className={insStyle.social} />
                <FaTelegramPlane className={insStyle.social} />
                <FaLinkedinIn className={insStyle.social} />
              </div>
              More than twenty years of experience in software architecture,
              design, development, implementation, and integration. Current
              focus is on mobile and web application development, Artificial
              Intelligence (AI) and Data Sciences, Blockchain, Internet of
              Things (IoT) and cloud native services. Over fifteen years of
              experience in leading a not-for-profit educational movement.
              Taught computer science and business subjects to over 10,000
              students worldwide. Triple Masters Degrees in engineering (MSE),
              business (MBA), and accounting information systems (MAC) from
              Arizona State University (ASU). Master of Arts (M.A.) in Economics
              from KU. Professional qualifications include Certified Public
              Accountant (CPA) and Certified Management Accountant (CMA). For
              eight consecutive years in 2007-2014 received the Microsoft’s Most
              Valuable Professional (MVP) Award (Client App MVP for 5 years and
              Cloud MVP for 2 years).
            </div>

            <div class={insStyle.cardcontainer}>
              <span class={insStyle.pro}>4IR</span>
              <img class={insStyle.round} src={fac3} alt="user" />
              <h3>ADIL ALTAF</h3>
              <h6>Head Instructor 4IR</h6>
              <p>
                Design Thinking, Lean Startup, Agile & DevOps Innovation Leader
              </p>
              <div class={insStyle.button}>
                <FaFacebookF className={insStyle.social} />
                <FaTelegramPlane className={insStyle.social} />
                <FaLinkedinIn className={insStyle.social} />
              </div>
              Design Thinking, Lean Startup, Agile & DevOps Innovation Leader
              (16 years) helping organizations (Enterprise & Startups) leverage
              technology to shorten the value stream to provide customers access
              to products and services as quickly as possible and to enable
              continuous innovation & delivery by combining innovation,
              development, and operations. This highly experimental, iterative
              leadership approach produces incremental increases in business
              value, reduces Prisk and time to market significantly, and
              increases the satisfaction of stakeholders. In-depth, hands-on
              experience with Open Source DevOps tools including Docker,
              Kubernetes, Jenkins, Terrafor, Ansible, and Prometheus on the
              Multi-Cloud (including AWS). Scrum Master & Organizational Agility
              Coach skilled in revolutionizing the culture and mindset of
              people, teams, and organizations to create an environment for
              continuous innovation. A servant-leader with skills and experience
              to build high performing teams that deliver valuable results.
            </div>
            <div class={insStyle.cardcontainer}>
              <span class={insStyle.pro}>BCC</span>
              <img class={insStyle.round} src={zeeshan} alt="user" />
              <h3>ZEESHAN HANIF</h3>
              <h6>Head Instructor BlockChain</h6>
              <p>Certified BlockChain Developer</p>
              <div class={insStyle.button}>
                <FaFacebookF className={insStyle.social} />
                <FaTelegramPlane className={insStyle.social} />
                <FaLinkedinIn className={insStyle.social} />
              </div>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout. The
              point of using Lorem Ipsum is that it has a more-or-less normal
              distribution of letters, as opposed to using 'Content here,
              content here', making it look like readable English. Many desktop
              publishing packages and web page editors now use Lorem Ipsum as
              their default model text, and a search for 'lorem ipsum' will
              uncover many web sites still in their infancy. Various versions
              have evolved over the years, sometimes by accident, sometimes on
              purpose (injected humour and the like).
            </div>
          </div>
        </div>
      </Layout>
    </Fragment>
  )
}

export default Instructor
