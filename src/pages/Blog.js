import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles, Card, CardMedia, CardActions, CardContent, Typography, createMuiTheme, ThemeProvider } from '@material-ui/core';
import { Link } from 'gatsby';
import blogImg from "../images/image1.jpg"
const theme = createMuiTheme({
    overrides: {
      MuiCardContent: {
        root: {
          paddingBottom: '0',
        },
      },
    },
  });

const useStyles = makeStyles(() => ({
    grid: {
        backgroundColor: "#fdf6ea",
        padding: "80px 0"
    },
    card: {
        height: "auto",
        width: "auto",
        variant: "outlined",
        backgroundColor: "#fdf6ea",
        boxShadow: "none",
        margin: "10px 10px"
    },
    media: {
        height: 280,
        width: 380,

    },
    blog: {
        marginBottom: "50px",
    },
    cardActions: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        flex: "1"
    },
    cardContent: {
        border: "3px solid black",
        borderRadius: "5px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        padding: "5px"
    },
    text: {
        borderTop: '0.1px solid black',
        padding: "5px 5px 0",
        display: "flex",
        alignItems: "center" 
    },
    tag: {
        display: "flex",
        flexDirection: "row",
        padding: "25px 0 5px",
        fontSize: "18px"
    },
    border: {
        borderRight: "2px solid rgba(0, 0, 0, 0.54)", 
        padding: "0 5px",
        borderRadius: "3px",
    }
}));

export default function Blog() {
    const classes = useStyles();

    return (
        <>
            <Grid className={classes.grid} container>
                <Grid item xs={12}>
                <Typography className={classes.blog} variant="h2" component="h1" align="center">Blog</Typography>
                    <Grid container justify="center">
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.media}
                                image={blogImg}
                                title="Lorem Ipsum"
                            >
                            </CardMedia>
                            <CardActions className={classes.cardActions}>
                                <ThemeProvider theme={theme}>
                                    <CardContent className={classes.cardContent}>
                                        <Typography style={{padding: "5px"}} variant="h5" component="h2">Mid</Typography>
                                        <Typography className={classes.text} color="textSecondary">Icon</Typography>
                                    </CardContent>
                                    <CardContent>
                                        <Typography style={{paddingTop: "10px"}} variant="h4">
                                        <Link to="/Blog/Gallery" style={{textDecoration: "none"}}>Title</Link></Typography>
                                        <div className={classes.tag}>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography style={{padding: "0 5px"}} variant="middle" color="TextSecondary">text</Typography>
                                        </div>
                                    </CardContent>
                                </ThemeProvider>
                            </CardActions>
                        </Card>
                        
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.media}
                                image={blogImg}
                                title="Lorem Ipsum"
                            />
                            <CardActions className={classes.cardActions}>
                                <ThemeProvider theme={theme}>
                                    <CardContent className={classes.cardContent}>
                                        <Typography style={{padding: "5px"}} variant="h5" component="h2">Mid</Typography>
                                        <Typography className={classes.text} color="textSecondary">Icon</Typography>
                                    </CardContent>
                                    <CardContent>
                                        <Typography style={{paddingTop: "10px"}} variant="h4">
                                        <Link to="/Blog/Gallery" style={{textDecoration: "none"}}>Title</Link></Typography>
                                        <div className={classes.tag}>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography style={{padding: "0 5px"}} variant="middle" color="TextSecondary">text</Typography>
                                        </div>
                                    </CardContent>
                                </ThemeProvider>
                            </CardActions>
                        </Card>

                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.media}
                                image={blogImg}
                                title="Lorem Ipsum"
                            />
                            <CardActions className={classes.cardActions}>
                                <ThemeProvider theme={theme}>
                                    <CardContent className={classes.cardContent}>
                                        <Typography style={{padding: "5px"}} variant="h5" component="h2">Mid</Typography>
                                        <Typography className={classes.text} color="textSecondary">Icon</Typography>
                                    </CardContent>
                                    <CardContent>
                                        <Typography style={{paddingTop: "10px"}} variant="h4">
                                        <Link to="/Blog/Gallery" style={{textDecoration: "none"}}>Title</Link></Typography>
                                        <div className={classes.tag}>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography className={classes.border} variant="middle" color="TextSecondary">text</Typography>
                                            <Typography style={{padding: "0 5px"}} variant="middle" color="TextSecondary">text</Typography>
                                        </div>
                                    </CardContent>
                                </ThemeProvider>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Grid>
        </>
        
    )
}
