import React, { Fragment } from 'react';
import insStyle from './css/Instructor.module.css';
import Layout from '../components/Layout/Layout'
import { Box, Typography } from '@material-ui/core';
import fac2 from '../images/instructor/zia.jpg'
import fac3 from '../images/instructor/adil2.jpeg'
import { FaFacebookF, FaTelegramPlane, FaLinkedinIn } from 'react-icons/fa'

function Instructor() {
    return (
        <Fragment>
            <Layout>
                <Box className={insStyle.box}
                    data-sal="fade"
                    data-sal-delay="500"
                    data-sal-easing="ease"
                    data-sal-duration="1000"
                >
                    <Typography variant="h2" className={insStyle.boxHeading} >
                        Instructor
                   </Typography>
                </Box>
                <div className={insStyle.FacultyContent}>
                    <p className={insStyle.para}> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                    <div>
                        <div className={insStyle.FacultyBanner}>
                            LEARN FROM THE INDUSTRY EXPERT'S
                </div>
                    </div>
                    <div className={insStyle.FacultyMember}>

                        <div class={insStyle.cardcontainer}>

                            <span class={insStyle.pro}>HEAD</span>
                            <img class={insStyle.round} src={fac2} alt="user" />
                            <h3>ZIA KHAN</h3>
                            <h6>PROGRAM HEAD</h6>
                            <p>MSE, MBA, MAC & M.A Economics</p>
                            <div class={insStyle.button}>
                                <FaFacebookF className={insStyle.social} />
                                <FaTelegramPlane className={insStyle.social} />
                                <FaLinkedinIn className={insStyle.social} />
                            </div>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                        </div>

                        <div class={insStyle.cardcontainer}>

                            <span class={insStyle.pro}>4IR</span>
                            <img class={insStyle.round} src={fac3} alt="user" />
                            <h3>ADIL ALTAF</h3>
                            <h6>Head Instructor 4IR</h6>
                            <p>Design Thinking, Lean Startup, Agile & DevOps Innovation Leader</p>
                            <div class={insStyle.button}>
                                <FaFacebookF className={insStyle.social} />
                                <FaTelegramPlane className={insStyle.social} />
                                <FaLinkedinIn className={insStyle.social} />
                            </div>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                        </div>
                    </div>
                </div>

            </Layout>
        </Fragment>
    )
}

export default Instructor;