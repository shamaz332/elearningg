import React from "react"
import AccountCircle from "@material-ui/icons/AccountCircle"
import "semantic-ui-css/semantic.min.css"
import { Link } from "gatsby"
import {
  Grid,
  Form,
  Segment,
  Button,
  Header,
  Image,
  Message,
  GridColumn,
  FormInput,
} from "semantic-ui-react"
import Layout from "../components/Layout/Layout.js"
function Signup() {
  return (
    <Layout>
      <div style={{ backgroundColor: "#d1e8e2", marginTop: "3.5em" }}>
        <Grid
          textAlign="center"
          style={{ height: "100vh" }}
          verticalAlign="middle"
        >
          <GridColumn style={{ maxWidth: 450 }}>
            <Header as="h2" textAlign="center" style={{ color: "#106466" }}>
              {/* <Image src="https://2ymyoh1qntrcues0m1wz77rh-wpengine.netdna-ssl.com/wp-content/themes/lms/images/logo.png" />{" "} */}
              <AccountCircle style={{ color: "#116466", fontSize: "4rem" }} />{" "}
              Register
            </Header>
            <Form size="large">
              <Segment stacked>
                <FormInput
                  fluid
                  name="username"
                  placeholder="Username"
                  type="text"
                />
                <FormInput
                  fluid
                  name="email"
                  placeholder="Email"
                  type="email"
                />
                <FormInput
                  fluid
                  name="password"
                  placeholder="Password"
                  type="password"
                />
                <FormInput
                  fluid
                  name="passwordConfirmation"
                  placeholder="Password Confirmation"
                  type="password"
                />

                <Button
                  fluid
                  style={{ backgroundColor: "#106466", color: "white" }}
                >
                  Submit
                </Button>
              </Segment>

              <Message>
                Already a Member? <Link to="/login">Login</Link>
              </Message>
            </Form>
          </GridColumn>
        </Grid>
      </div>
    </Layout>
  )
}
export default Signup
