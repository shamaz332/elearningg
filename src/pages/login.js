import React from "react"
import "semantic-ui-css/semantic.min.css"
import AccountCircle from "@material-ui/icons/AccountCircle"
import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
} from "semantic-ui-react"
import { Link } from "gatsby"
import Layout from "../components/Layout/Layout.js"
const Login = () => (
  <div style={{ backgroundColor: "#d1e8e2" }}>
    <Layout>
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" style={{ color: "#106466" }} textAlign="center">
            {/* <Image src={require("../images/header/4ir_logo.png")} /> */}
            <AccountCircle
              style={{ color: "#116466", fontSize: "4rem" }}
            />{" "}
            Log-in to your account
          </Header>
          <Form size="large">
            <Segment stacked>
              <Form.Input fluid placeholder="E-mail address" />
              <Form.Input fluid placeholder="Password" type="password" />

              <Button
                style={{ backgroundColor: "#106466", color: "white" }}
                fluid
                size="large"
              >
                Login
              </Button>
            </Segment>
          </Form>
          <Message>
            New to us? <Link to="/signup">Sign Up</Link>
          </Message>
        </Grid.Column>
      </Grid>
    </Layout>
  </div>
)

export default Login
