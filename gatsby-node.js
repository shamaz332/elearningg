const path = require(`path`)
// Log out information after a build is done
exports.onPostBuild = ({ reporter }) => {
  reporter.info(`Your Gatsby site has been built!`)
}
// Create blog pages dynamically
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const blogPostTemplate = path.resolve(`./src/templates/temp.js`)
  const result = await graphql(`
  {
    allContentfulCourse {
      edges {
        node {
          title
          id
          
          
        }
      }
    }
  }
  
  `)
  result.data.allContentfulCourse.edges.forEach(edge => {
    createPage({
      path: `/Courses/${edge.node.title}`,
      component: blogPostTemplate,
      context: {
        title: edge.node.title,
        id:edge.node.id,
       
      },
    })
  })
  
}
